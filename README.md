Broomlight
=========

Broomlight adalah aplikasi mobile untuk mengatur penggunaan listrik rumah secara real time. Dengan aplikasi broomlight kita bisa mematikan atau menyalakan perangkat yang menggunakan listrik di rumah kita hanya dengan login melalui perangkat mobile dimana pun dan kapan pun melalui koneksi internet. Pengguna yang berada dalam satu rumah dapat mengatur perangkat listrik, melihat penggunaan listrik, mengatur jadwal penggunaan listrik dan chatting dalam aplikasi broomlight. 

Instalasi
--------

1. Install Node.js
2. Install Sails js (`npm install sails`)
3. cd ke direktorinya
4. `node app`.