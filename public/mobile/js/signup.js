function signup() {
	var defaultMap = {
    latitude: -6.9147444,
    longitude: 107.6098111
  };
	$("#map").goMap({ 
	  latitude: defaultMap.latitude,
	  longitude: defaultMap.longitude,
	  maptype: 'ROADMAP',
	  disableDoubleClickZoom: true,
	  zoom: 14 
	}); 
	setTimeout(function() {
		$("#map").hide().fadeIn('slow', function() {
		  google.maps.event.trigger($.goMap.map, 'resize');
		  var center = new google.maps.LatLng(defaultMap.latitude,defaultMap.longitude);
		  $.goMap.map.setCenter(center);
		});
	}, 100);
	$('#addMapButton').click(function () {
		$.goMap.createMarker({
			address: 'Bandung, Indonesia',
			title: '',
			html: {
				content: 'Drag ke alamat rumah baru yang dituju,<br> lalu tuliskan nama rumah baru',
				popup: true 
			},
			draggable: true,
			id: 'marker-new',
			icon: 'images/green-dot.png'
		});
		$(this).hide();
		$('#rumahName').removeAttr('disabled');
		$('#rumahName').textinput('enable');
		$('#rumahName').focus();
		$('#rumahLat').val(defaultMap.latitude);
    $('#rumahLng').val(defaultMap.longitude);
		$.goMap.createListener({type:'marker', marker: 'marker-new'}, 'dragend', function() { 
	    $('#rumahName').val($('#map').data('marker-new').getTitle());
	    var pos = $('#map').data('marker-new').getPosition();
	    $('#rumahLat').val(pos.lat());
	    $('#rumahLng').val(pos.lng());
	    $('#rumahName').focus();
		}); 
		google.maps.event.trigger($('#map').data('marker-new'), 'dragend');
	});
	$.get(server + 'rumah', function(data) {
		for(var i = 0, l = data.length; i < l; i++) {
			var marker = data[i];
			addMarker('marker-'+marker.id, marker.lat, marker.long, marker.nama);
		}
	}, 'json');
	function addMarker(myId, lat, lng, myTitle)
	{
		$.goMap.createMarker({
			id: myId,
			latitude: lat,
			longitude: lng,
			title: myTitle
		});
		$.goMap.createListener({type:'marker', marker: myId}, 'click', function() { 
	    $('#rumahName').val($('#map').data(myId).getTitle());
	    var pos = $('#map').data(myId).getPosition();
	    $('#rumahLat').val(pos.lat());
	    $('#rumahLng').val(pos.lng());
		}); 
	}
}