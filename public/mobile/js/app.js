var SailsCollection = Backbone.Collection.extend({ 
	socket: null, 
	model: {},
	urlname: '',
	initialize: function () {
		_.bindAll(this, 'onConnect', 'onRequest', 'onMessage');
	},
	onConnect: function () {
		this.socket.request('/' + this.urlname, {}, this.onRequest);
		this.socket.on('message', this.onMessage);
	},
	onRequest: function (messages) {
		this.set(messages);
	},
	onMessage: function (messages) {
		var m = messages.uri;
		if (m.indexOf('create') != -1) {
			if (messages.data.groupId == userProfile.user.group) 
				this.add(messages.data);
		} else if (m.indexOf('update') != -1) {
			if (messages.data.groupId == userProfile.user.group) 
				this.get(messages.data.id).set(messages.data);
		} else if (m.indexOf('destroy') != -1) {
			this.remove(this.get(messages.data.id));
		}
	},
	sync: function(method, model, options) { 
		this.socket = io.connect(server, {'force new connection': true});
		this.socket.on('connect', this.onConnect);
	} 
}); 