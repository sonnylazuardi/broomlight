var userProfile = {};
var managementState = false;
var LoadingView = Marionette.ItemView.extend({
  template: "#templateLoading"
});

_.templateSettings = { 
	evaluate    : /\{\{([\s\S]+?)\}\}/g, 
  interpolate : /\{\{=([\s\S]+?)\}\}/g, 
  escape      : /\{\{-([\s\S]+?)\}\}/g 
}; 

function showServer() {
	$('#serverBox').show();
}

$('#loginForm').submit(function() { 
	var username = $('#username').val(); 
	var password = $('#password').val(); 
	// checkConnection();
	if (username && password) {
		$.post( 
			server + 'login', 
			{username: username, password: password}, 
			function (response) { 
				userProfile = response;
				$('.userFooter').html(username);
				menu();
				window.location = '#menu'; 
			} 
		).fail(function(res) { 
			if ([400, 404].indexOf(res.status) != -1) {
				alert('Username atau password salah');
			} else {
				alert(res.status + ', Login error');
			}
		}); 
	} else { 
		alert('Username dan password harus diisi'); 
	} 
	return false; 
}); 

function validateEmail(email) { 
  var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return re.test(email);
} 

$('#signupForm').submit(function(){
	var username = $('#signupName').val();
	var password = $('#signupPassword').val();
	var myrumah = $('#rumahName').val();
	var myemail = $('#emailName').val();
	var mylat = $('#rumahLat').val();
	var mylng = $('#rumahLng').val();
	var confirmPassword = $('#signupConfirmPassword').val();
	// checkConnection();
	if (username && password && myrumah && myemail) {
		if (password !== confirmPassword) {
			alert("Password tidak cocok");
		} else if (!validateEmail(myemail)) {
			alert("Email tidak valid");
		} else {
			$.post(
				server + 'signup',
				{username: username, password: password,
					myrumah: myrumah, mylat :mylat, mylng:mylng, email: myemail},
				function(response) {
					userProfile = response;
					$('.userFooter').html(username);
					menu();
					window.location = '#menu';
				}
			).fail(function(res) {
				alert('Error: '+res.getResponseHeader('error'));
			});
		}
	} else {
		alert("Username, Password, Rumah, dan Email tidak boleh kosong");
	}
	return false;
});

function logout() {
	$.get(server + 'logout'); 
	userProfile = {};
	$('#username').val('');
	$('#password').val('');
	window.location = '#login';
	return false; 
}