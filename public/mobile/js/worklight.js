window.$ = window.jQuery = WLJQ;
WL.App.overrideBackButton(function(){
  WL.App.close();
});

var wlInitOptions = {
    connectOnStartup : false
};

if (window.addEventListener) {
  window.addEventListener('load', function() { WL.Client.init(wlInitOptions); }, false);
} else if (window.attachEvent) {
  window.attachEvent('onload',  function() { WL.Client.init(wlInitOptions); });
}

function wlCommonInit(){
	// Common initialization code goes here
	document.addEventListener("backbutton", onBackKeyDown, false);
	document.addEventListener("deviceready", onDeviceReady, false);
}

function onBackKeyDown() {
	WL.App.close();
}

function onDeviceReady() {
  // Register the event listener
  checkConnection();
}

function checkConnection() {
	var networkState = navigator.connection.type;

  var states = {};
  states[Connection.UNKNOWN]  = 'Unknown connection';
  states[Connection.ETHERNET] = 'Ethernet connection';
  states[Connection.WIFI]     = 'WiFi connection';
  states[Connection.CELL_2G]  = 'Cell 2G connection';
  states[Connection.CELL_3G]  = 'Cell 3G connection';
  states[Connection.CELL_4G]  = 'Cell 4G connection';
  states[Connection.NONE]     = 'No network connection';

  if (states[networkState] == 'No network connection') {
  	alert('Tidak ada koneksi internet, Aplikasi ini membutuhkan koneksi internet');
  	WL.App.close();
  }
}