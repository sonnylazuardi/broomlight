function menu() {
	$("#mapProfile").goMap({ 
	  latitude: userProfile.rumah.lat,
	  longitude: userProfile.rumah.long,
	  disableDoubleClickZoom: true,
	  zoom: userProfile.rumah.zoom,
	  maptype: 'ROADMAP'
	}); 
	$.goMap.createMarker({
		id: 'marker',
		latitude: userProfile.rumah.lat,
		longitude: userProfile.rumah.long,
		title: userProfile.rumah.nama
	});
	setTimeout(function(){
		$("#mapProfile").hide().fadeIn('slow', function() {
		  google.maps.event.trigger($.goMap.map, 'resize');
		  var center = new google.maps.LatLng(userProfile.rumah.lat, userProfile.rumah.long);
		  $.goMap.map.setCenter(center);
		});
	}, 100);
	$('#profpic').html('<div style="background:url('+userProfile.profpic+') no-repeat; background-size: cover; height:250px; width:100%"></div>');
	$('#profileNama').html(userProfile.user.username);
	$('#rumahNama').html(userProfile.rumah.nama);
}