function chat() {
	var chat = {};
	//Model
	var MessageModel = Backbone.Model.extend({ 
		urlRoot: '/messages' 
	}); 

	var MessageCollection = SailsCollection.extend({
		urlname: 'messages',
		model: MessageModel
	});

	//View
	var MessagesView = Marionette.ItemView.extend({ 
		template: '#templateChat',
		className: 'chat-item ui-grid-a'
	}); 

	var MessagesViewList = Marionette.CollectionView.extend({
		emptyView: LoadingView,
		itemView: MessagesView,
		onAfterItemAdded: function () {
			setTimeout(function () {
				$('div.scroller.chat-scroll').scrollTop($('div.scroller.chat-scroll')[0].scrollHeight);
			}, 500);
		}
	});

	chat.messagesList = new MessagesViewList();
	$('#messagesContainer').html(chat.messagesList.render().$el);

	$.get(server + 'messages/findAll', {}, function(messages) {
		chat.messages = new MessageCollection(messages);
		chat.messagesList = new MessagesViewList({
			collection: chat.messages
		});
		chat.messages.fetch();
		$('#messagesContainer').html(chat.messagesList.render().$el);
		
	});

	$('#postMessageButton').click(function() { 
		var messageText = $('#message').val(); 
		if (messageText != '') {
			$.post(
				server + 'messages/create', 
				{message: messageText}
			); 
		}
		$('#message').val(''); 
	}); 
}