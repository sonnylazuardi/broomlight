function management() {
	var power = {};

	//Model
	var PowerModel = Backbone.Model.extend({ 
		urlRoot: '/power' 
	}); 

	var PowerCollection = SailsCollection.extend({
		urlname: 'power',
		model: PowerModel
	});
	
	//View
	var PowersView = Marionette.ItemView.extend({ 
		tagName: 'li',
		className: 'ui-li ui-li-static ui-btn-up-a',
		attributes: {
			'data-role' : 'fieldcontain'
		},
		template: '#powerTemplate'
	}); 	

	var PowersViewList = Marionette.CollectionView.extend({
		tagName: 'ul',
		className: 'ui-listview ui-listview-inset ui-corner-all ui-shadow',
		emptyView: LoadingView,
		template: '#powerContainer',
		attributes: {
			'data-role': 'listview',
			'data-inset': true,
		},
		itemView: PowersView,
		initialize: function() {
			if (this.collection)
				this.collection.on('change', this.render, this);
		},
		onAfterItemAdded: function() {
			_.defer(function() {
				if ( $('.testing').hasClass('ui-slider-switch')) {
					$('.testing').slider('refresh');
				} else {
					$('.testing').slider({
						stop: function (event, ui) {
							$.post( 
								server + 'power/update', 
								{id: parseInt($(this).data('id')), status: parseInt($(this).val())}, function() {
							}); 
						} 
					});
				}
			});
		}
	});

	power.powersList = new PowersViewList();
	$('#powerContainer').html(power.powersList.render().$el);

	$.get(server + 'power/findAll', {}, function(powers) {
		power.powers = new PowerCollection(powers);
		power.powersList = new PowersViewList({
			collection: power.powers
		});
		power.powers.fetch();
		$('#powerContainer').html(power.powersList.render().$el);
		
	});

}