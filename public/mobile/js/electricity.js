function electricity() {
	var seriesPower;
	var group = userProfile.user.group;
 $('#container').highcharts({
    chart: {
        backgroundColor: null,
        // animation: Highcharts.svg,
        marginRight: 10,
        type: 'spline',
        events: {
            load: function() {
              seriesPower = this.series[0];
            }
        }
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'time',
        lineColor: '#fff',
        tickPixelInterval: 150
    },
    yAxis: {
        title: {
            text: 'Watt'
        },
        gridLineColor: '#fff',
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    },
    credits: {
        enabled: false
      },
    series: [{
    		name: userProfile.rumah.nama,
    		lineWidth: 7,
    		color: '#555',
    		marker: {
          lineWidth: 7,
          radius: 8
        },
        data: [
        	['', 0],
            ['', 0],
            ['', 0],
            ['', 0],
            ['', 0],
            ['', 0],
            ['', 0],
            ['', 0],
            ['', 0],
            ['', 0],
            ['', 0],
        ]
    }]
  });

	var socket = io.connect(server, {'force new connection': true});
	socket.on('connect', function() {
		socket.request('/electricity',{}, function (response) { 
		});
		socket.on('detik', function(msg) {
			if (group == msg.groupId) {
				var x = msg.timestamp;
				var y = msg.value;
				console.log(msg.timestamp);
				console.log(msg.value);
				seriesPower.addPoint([x, y], true, true);
			}
		});
	});
}