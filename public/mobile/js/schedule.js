function schedule() {
	var schedule = {};
	//Model
	var ScheduleModel = Backbone.Model.extend({
		urlRoot: '/schedules'
	});
	
	var ScheduleCollection = SailsCollection.extend({
		urlname: 'schedules',
		model: ScheduleModel
	});

	//View
	var SchedulesView = Marionette.ItemView.extend({ 
		tagName: 'li',
		className: 'ui-li ui-li-static ui-btn-up-a',
		attributes: {
			'data-role' : 'fieldcontain'
		},
		template: '#scheduleTemplate'
	}); 

	var SchedulesViewList = Marionette.CollectionView.extend({
		tagName: 'ul',
		className: 'ui-listview ui-listview-inset ui-corner-all ui-shadow',
		emptyView: LoadingView,
		attributes: {
			'data-role': 'listview',
			'data-inset': true
		},
		itemView: SchedulesView,
		initialize: function() {
			if (this.collection)
				this.collection.on('change', this.render, this);
		},
		onAfterItemAdded: function(itemView){
			_.defer(function() {
				$(".deleteSchedule", this.$el).buttonMarkup( { inline: true, icon: "delete"} );
			});	
	  }
	});

	schedule.schedulesList = new SchedulesViewList();
	$('#scheduleContainer').html(schedule.schedulesList.render().$el);

	$.get(server + 'schedules/findAll', {}, function(schedules) {
		schedule.schedules = new ScheduleCollection(schedules);
		schedule.schedulesList = new SchedulesViewList({
			collection: schedule.schedules
		});
		schedule.schedules.fetch();
		$('#scheduleContainer').html(schedule.schedulesList.render().$el);
	});
}

function deleteSchedule(id)
{
	$.post(
		server + 'schedules/destroy',
		{id:id}
	);
}