function formSchedule() {
	$.post(
		server + 'power/index', {},
		function (result) {
			$('#powerId').empty();
			for(var i = 0, l = result.length; i < l; i++) {
				var perangkat = result[i];
				$('#powerId').append("<option value='"+perangkat.portId+"'>"+perangkat.name+"</option>");
			}
		}
	);
}
$(".dial").knob();

$('#scheduleForm').submit(function() {
	if (powerId) {
		$.post(
			server + 'schedules/create',
			$(this).serialize(),
			function (result) {
				schedule();
				window.location = '#schedule';
			}
		);
	} else alert('Nama Perangkat tidak boleh kosong');
	return false;
});