define(function (require) {
	"use strict";

	var Marionette = require('marionette'),
			tpl = require('text!tpl/Login.html');

	return Marionette.ItemView.extend({
		initialize: function () {
			_.bindAll(this, 'loginCheck');
			this.render();
		},

		template: _.template(tpl),

		events: {
			'submit #loginForm': 'loginCheck'
		},

		loginCheck: function () {
			var username = $('#username').val(); 
			var password = $('#password').val();
			// checkConnection();
			if (username && password) {
				$.post( 
					server + 'login', 
					{username: username, password: password}, 
					function (response) { 
						console.log(response);
						userProfile = response;
						window.location.href = '#menu';
					}
				).fail(function(res) {
					if ([400, 404].indexOf(res.status) != -1) {
						alert('Username atau password salah');
					} else {
						alert('Login error');
					}
				}); 
			} else { 
				alert('Username dan password harus diisi'); 
			} 
			return false;
		},

	});

});