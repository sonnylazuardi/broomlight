define(function (require) {

  var $ = require('jquery'),
      gomap = require('jquery.gomap'),
      Marionette = require('marionette'),
      tpl = require('text!tpl/Signup.html');

  return Marionette.ItemView.extend({

    defaultMap: {
      latitude: -6.9147444,
      longitude: 107.6098111
    },

    template: _.template(tpl),

    initialize: function () {
      _.bindAll(this, 'renderMap', 'addMap');
      this.render();
    },

    events : {
      'click #addMapButton': 'addMap'
    },

    addMap: function (e) {
      e.preventDefault();
      $.goMap.createMarker({
        address: 'Bandung, Indonesia',
        title: '',
        html: {
          content: 'Drag ke alamat rumah baru yang dituju,<br> lalu tuliskan nama rumah baru',
          popup: true 
        },
        draggable: true,
        id: 'marker-new',
        icon: 'images/green-dot.png'
      });
      $(e.currentTarget).hide();
      $('#rumahName').removeAttr('disabled');
      $('#rumahLat').val(this.defaultMap.latitude);
      $('#rumahLng').val(this.defaultMap.longitude);
      $('#rumahName').focus();
      $.goMap.createListener({type:'marker', marker: 'marker-new'}, 'dragend', function() { 
        // alert('pindah');
        $('#rumahName').val($('#map').data('marker-new').getTitle());
        var pos = $('#map').data('marker-new').getPosition();
        $('#rumahLat').val(pos.lat());
        $('#rumahLng').val(pos.lng());
        $('#rumahName').focus();
      }); 
      google.maps.event.trigger($('#map').data('marker-new'), 'dragend');
    },

    renderMap: function () {
      $("#map").goMap({ 
        latitude: this.defaultMap.latitude,
        longitude: this.defaultMap.longitude,
        maptype: 'ROADMAP',
        disableDoubleClickZoom: true,
        zoom: 14 
      }); 
      $.get(server + 'rumah', function(data) {
        for(var i = 0, l = data.length; i < l; i++) {
          var marker = data[i];
          var myId = 'marker-' + marker.id,
              lat = marker.lat, 
              lng = marker.long,
              myTitle = marker.nama;
          $.goMap.createMarker({
            id: myId,
            latitude: lat,
            longitude: lng,
            title: myTitle
          });
          $.goMap.createListener({type:'marker', marker: myId}, 'click', function() { 
            $('#rumahName').val($('#map').data(myId).getTitle());
            var pos = $('#map').data(myId).getPosition();
            $('#rumahLat').val(pos.lat());
            $('#rumahLng').val(pos.lng());
          });
        }}, 'json');
    },

    onRender: function () {
      _(this.renderMap).defer();
    },

  });

});