define(function (require) {
	'use strict';

	var Marionette = require('marionette'),
			LoadingView = require('app/views/Loading'),
			tpl = require('text!tpl/Chat.html'),
			tplItem = require('text!tpl/ChatItem.html'),
			models = require('app/models/chat');

	var ChatView = Marionette.ItemView.extend({
		initialize: function () {
			this.render();
		},
		className: 'messageBox grid',
		template: _.template(tplItem),
	});

	var ChatListView = Marionette.CollectionView.extend({
		itemView: ChatView,
		emptyView: LoadingView,
		initialize: function (){
			_.bindAll(this, 'scrollDown');
		},
		scrollDown: function() {
			$('.content').scrollTop($('.content')[0].scrollHeight);
		},
		onAfterItemAdded: function () {
			_(this.scrollDown).defer();
		}
	});

	return Marionette.ItemView.extend({

		initialize: function () {
			var that = this;
			$.get(server + 'messages/findAll', {}, function(messages) {
				that.chatList = new models.ChatCollection(messages);	
				that.chatList.fetch();
				that.render();
			});
			that.render();
		},

		template: _.template(tpl),

		onRender: function () {
			var listView = new ChatListView({
				collection: this.chatList
			});
			$('#messagesContainer', this.$el).html(listView.render().$el);
		},

	});
});