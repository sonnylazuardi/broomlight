define(function (require) {
	'use strict';

	var $ = require('jquery'),
			gomap = require('jquery.gomap'),
			Marionette = require('marionette'),
			tpl = require('text!tpl/Menu.html');

	return Marionette.ItemView.extend({

		initialize: function () {
			_.bindAll(this, 'logout', 'renderProfile');
			this.render();
		},

		template: _.template(tpl),

		events: {
			'click #logout': 'logout'
		},

		logout: function(e) {
			e.preventDefault();
			$.post( server + 'logout' );
			userProfile = {};
			window.location.href = '#login';
		},

		renderProfile: function () {
			$("#mapProfile").goMap({ 
			  latitude: userProfile.rumah.lat,
			  longitude: userProfile.rumah.long,
			  disableDoubleClickZoom: true,
			  zoom: userProfile.rumah.zoom,
			  maptype: 'ROADMAP'
			}); 
			$.goMap.createMarker({
				id: 'marker',
				latitude: userProfile.rumah.lat,
				longitude: userProfile.rumah.long,
				title: userProfile.rumah.nama
			});
			$('#profpic').html('<div style="background:url('+userProfile.profpic+') no-repeat; background-size: cover; height:250px; width:100%"></div>');
			$('#profileNama').html(userProfile.user.username);
			$('#rumahNama').html(userProfile.rumah.nama);
		},

		onRender: function () {
			_(this.renderProfile).defer();
		},

	});
});