define(function (require) {
	'use strict';

	var Marionette = require('marionette'),
			tpl = require('text!tpl/Loading.html');

	return Marionette.ItemView.extend({
		initialize: function () {
			this.render();
		},
		template: _.template(tpl),
	});
});