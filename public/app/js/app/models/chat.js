define(function (require) {
	'use strict';
	var $ = require('jquery'),
			Backbone = require('backbone'), 
			Marionette = require('marionette'),
			socket = require('socket'), 
			urlname = 'messages';

	var Chat = Backbone.Model.extend({
		urlRoot: '/messages'
	});

	var ChatCollection = Backbone.Collection.extend({
		socket: null,
		model: Chat,
		initialize: function () {
			_.bindAll(this, 'onConnect', 'onRequest', 'onMessage');
		},
		onConnect: function () {
			this.socket.request('/' + urlname, {}, this.onRequest);
			this.socket.on('message', this.onMessage);
		},
		onRequest: function (messages) {
			this.set(messages);
		},
		onMessage: function (messages) {
			var m = messages.uri;
			if (m.indexOf('create') != -1) {
				this.add(messages.data);
			} else if (m.indexOf('update') != -1) {
				this.get(messages.data.id).set(messages.data);
			} else if (m.indexOf('destroy') != -1) {
				this.remove(this.get(messages.data.id));
			}
		},
		sync: function(method, model, options) {
			this.socket = io.connect(server, {'force new connection': true});
			this.socket.on('connect', this.onConnect);
		}
	});

	return {
		Chat: Chat,
		ChatCollection: ChatCollection
	};
	
});