define(function (require) {

	"use strict";

	var $  = require('jquery'),
		Backbone  = require('backbone'),
		PageSlider = require('app/utils/pageslider'),
		LoginView  = require('app/views/Login'),
		SignupView  = require('app/views/Signup'),
		MenuView  = require('app/views/Menu'),
		ChatView  = require('app/views/Chat'),

		slider = new PageSlider($('body')),

		loginView = new LoginView();



	return Backbone.Router.extend({

		routes: {
			 "": "login",
			"login": "login",
			"signup": "signup",
			"menu": "menu",
			"chat": "chat"
		},

		route: function(route, name, callback) {
	    var router = this;
	    if (!callback) callback = this[name];
	    var f = function() {
	      if (['menu', 'chat'].indexOf(name) != -1) {
	      	if (!$.isEmptyObject(userProfile)) 
	      		callback.apply(router, arguments);
	      	else
	      		window.location.href = '#login';
	      } else
	      	callback.apply(router, arguments);
	    };
	    return Backbone.Router.prototype.route.call(this, route, name, f);
	  },

		login: function () {
			loginView.delegateEvents();
			// slider.slidePage(loginView.$el);
			$('body').html(loginView.$el);
		},

		signup: function() {
			// slider.slidePage(new SignupView().$el);
			$('body').html(new SignupView().$el);
		},

		menu: function () {
			// slider.slidePage(new MenuView().$el);
			$('body').html(new MenuView().$el);
		},

		chat: function () {
			var chatView = new ChatView();
			// slider.slidePage(chatView.$el);
			$('body').html(chatView.$el);
		},

	});

});