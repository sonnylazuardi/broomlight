require.config({

	baseUrl: 'js/lib',

	paths: {
		app: '../app',
		tpl: '../tpl'
	},

	shim: {
		'backbone': {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},
		'underscore': {
			exports: '_'
		},
    'marionette': {
      deps: ['backbone'],
      exports: 'Marionette'
    },
    'jquery.gomap': ['jquery'],
	}
});

require(['jquery', 'backbone', 'app/router'], function ($, Backbone, Router) {

	var router = new Router();
	Backbone.history.start();
});

//Global Variables
var userProfile = {};
//jangan lupa diganti
var server = 'http://localhost:8000/';