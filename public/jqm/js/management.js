function management() {
	var SailsCollection = Backbone.Collection.extend({ 
		sailsCollection: '',
		socket: null, 
		sync: function(method, model, options) { 
			var where = {}; 
			if (options.where) { 
				where = { 
					where: options.where 
				} 
			}
			if (typeof this.sailsCollection === 'string' && this.sailsCollection !== '') { 
				// alert('tes3');
				this.socket = io.connect(server, {'force new connection': true});
				// alert('tes4');
				this.socket.on('connect', _.bind(function(){ 
					this.socket.request('/' + this.sailsCollection, where, _.bind(function(users){ 
						// alert('tes2');
						this.set(users); 
					}, this)); 
	 
					this.socket.on('message', _.bind(function(msg){ 
						var m = msg.uri;
						if (m.indexOf("create") !== -1) { 
							this.add(msg.data); 
						} else if (m.indexOf("update") !== -1) { 
							this.get(msg.data.id).set(msg.data); 
						} else if (m.indexOf("destroy") !== -1){ 
							this.remove(this.get(msg.data.id)); 
						} 
					}, this)); 
				}, this)); 
			} else { 
				alert('Error: Cannot retrieve models because property "sailsCollection" not set on the collection');
			} 
		} 
	}); 
	 
	var PowerModel = Backbone.Model.extend({ 
		urlRoot: '/power' 
	}); 
	var PowerCollection = SailsCollection.extend({ 
		sailsCollection: 'power', 
		model: PowerModel 
	}); 

	var powers = new PowerCollection(); 

	powers.fetch(); 

	var PowersView = Marionette.ItemView.extend({ 
		tagName: 'li',
		className: 'ui-li ui-li-static ui-btn-up-a',
		attributes: {
			'data-role' : 'fieldcontain'
		},
		template: '#powerTemplate'
	}); 

	var PowersViewList = Marionette.CollectionView.extend({
		tagName: 'ul',
		className: 'ui-listview ui-listview-inset ui-corner-all ui-shadow',
		emptyView: LoadingView,
		template: '#powerContainer',
		attributes: {
			'data-role': 'listview',
			'data-inset': true,
		},
		itemView: PowersView,
		initialize: function() {
			this.collection.on('change', this.render, this);
		},
		onAfterItemAdded: function(itemView) {
			if ( itemView.$el.find('.testing').hasClass('ui-slider-switch')) {
				itemView.$el.find('.testing').slider('refresh');
			} else {
				itemView.$el.find('.testing').slider({
					stop: function (event, ui) {
						$.post( 
							server + 'power/update', 
							{id: parseInt($(this).data('id')), status: parseInt($(this).val())}, function() {
						});
						var model = powers.get(parseInt($(this).data('id')));
						model.set({status: parseInt($(this).val())}); 
						model.save(); 
					} 
				});
			}
		}
	});

	var powersList =  new PowersViewList({
		collection: powers
	});

	$('#powerContainer').html(powersList.render().$el);

}