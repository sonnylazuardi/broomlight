function formSchedule() {
	$(".dial").knob();
	$.post(
		server + 'power/index', {},
		function (result) {
			for(var i = 0, l = result.length; i < l; i++) {
				var perangkat = result[i];
				$('#powerId').append("<option value='"+perangkat.portId+"'>"+perangkat.name+"</option>");
			}
		}
	);
	$('#scheduleForm').submit(function() {
		if (powerId) {
			$.post(
				server + 'schedules/create',
				$(this).serialize(),
				function (result) {
					schedule();
					window.location = '#schedule';
				}
			).fail(function(res) {
				alert('Error: ' + res.getResponseHeader('error'));
			});
		} else alert('Nama Perangkat tidak boleh kosong');
		return false;
	});
}