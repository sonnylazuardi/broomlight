function chat() {
	var SailsCollection = Backbone.Collection.extend({ 
		sailsCollection: '', 
		socket: null, 
		sync: function(method, model, options) { 
			var where = {}; 
			if (options.where) { 
				where = { 
					where: options.where 
				} 
			} 
			if (typeof this.sailsCollection === 'string' && this.sailsCollection !== '') { 
				this.socket = io.connect(server, {'force new connection': true}); 
				this.socket.on('connect', _.bind(function(){ 
					this.socket.request('/' + this.sailsCollection, where, _.bind(function(users){ 
						this.set(users); 
					}, this)); 

					this.socket.on('message', _.bind(function(msg){ 
						var m = msg.uri;
						if (m.indexOf("create") !== -1) {
							this.add(msg.data);
						} else if (m.indexOf("update") !== -1) {
							this.get(msg.data.id).set(msg.data);
						} else if (m.indexOf("destroy") !== -1){
							this.remove(this.get(msg.data.id));
						}
					}, this)); 
				}, this)); 
			} else { 
				console.log('Error: Cannot retrieve models because property "sailsCollection" not set on the collection'); 
			} 
		} 
	}); 

	var MessageModel = Backbone.Model.extend({ 
		urlRoot: '/messages' 
	}); 
	var MessageCollection = SailsCollection.extend({ 
		sailsCollection: 'messages', 
		model: MessageModel 
	}); 
	var messages = new MessageCollection(); 
	messages.fetch(); 

	var MessagesView = Marionette.ItemView.extend({ 
		tagName: 'li',
		className: 'ui-li ui-li-static ui-btn-up-a',
		template: '#templateChat'
	}); 

	var MessagesViewList = Marionette.CollectionView.extend({
		tagName: 'ul',
		className: 'ui-listview ui-listview-inset ui-corner-all ui-shadow',
		emptyView: LoadingView,
		attributes: {
			'data-role': 'listview',
			'data-inset': true
		},
		itemView: MessagesView
	});

	var messagesList =  new MessagesViewList({
		collection: messages
	});

	$('#messagesContainer').html(messagesList.render().$el);

	$('#postMessageButton').click(function() { 
		var messageText = $('#message').val(); 
		$.post( 
			server + 'messages/create', 
			{message: messageText}, null
		).fail(function(res) { 
			alert('Error: '+res.getResponseHeader('error')); 
		}); 
		messages.create({message: messageText}, {wait: true}); 
		$('#message').val(''); 
	}); 
}