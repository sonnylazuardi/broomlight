/*
*  Licensed Materials - Property of IBM
*  5725-G92 (C) Copyright IBM Corp. 2006, 2013. All Rights Reserved.
*  US Government Users Restricted Rights - Use, duplication or
*  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/

function wlCommonInit(){
	// Common initialization code goes here
	document.addEventListener("backbutton", onBackKeyDown, false);
	document.addEventListener("deviceready", onDeviceReady, false);
}

function onBackKeyDown() {
	WL.App.close();
}

function onDeviceReady() {
  // Register the event listener
  checkConnection();
}

function checkConnection() {
	var networkState = navigator.connection.type;

  var states = {};
  states[Connection.UNKNOWN]  = 'Unknown connection';
  states[Connection.ETHERNET] = 'Ethernet connection';
  states[Connection.WIFI]     = 'WiFi connection';
  states[Connection.CELL_2G]  = 'Cell 2G connection';
  states[Connection.CELL_3G]  = 'Cell 3G connection';
  states[Connection.CELL_4G]  = 'Cell 4G connection';
  states[Connection.NONE]     = 'No network connection';

  if (states[networkState] == 'No network connection') {
  	alert('Tidak ada koneksi internet, Aplikasi ini membutuhkan koneksi internet');
  	WL.App.close();
  }
}