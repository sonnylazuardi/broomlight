function schedule() {
	var SailsCollection = Backbone.Collection.extend({
		sailsCollection: '',
		socket: null,
		sync: function(method, model, options) {
			console.log(model);
			var where = {};
			if (options.where) {
				where = {
					where: options.where
				}
			}
			if (typeof this.sailsCollection === 'string' && this.sailsCollection !== '') {
				this.socket = io.connect(server, {'force new connection': true});
				this.socket.on('connect', _.bind(function(){
					this.socket.request('/' + this.sailsCollection, where, _.bind(function(users){
						this.set(users);
					}, this));

					this.socket.on('message', _.bind(function(msg){
						var m = msg.uri;
						if (m.indexOf("create") !== -1) {
							this.add(msg.data);
						} else if (m.indexOf("update") !== -1) {
							this.get(msg.data.id).set(msg.data);
						} else if (m.indexOf("destroy") !== -1){
							this.remove(this.get(msg.data.id));
						}
					}, this));
				}, this));
			} else {
				console.log('Error: Cannot retrieve models because property "sailsCollection" not set on the collection');
			}
		}
	});

	var ScheduleModel = Backbone.Model.extend({
		urlRoot: '/schedules'
	});
	var ScheduleCollection = SailsCollection.extend({
		sailsCollection: 'schedules',
		model: ScheduleModel
	});
	var schedules = new ScheduleCollection();
	schedules.fetch();

	var SchedulesView = Marionette.ItemView.extend({ 
		tagName: 'li',
		className: 'ui-li ui-li-static ui-btn-up-a',
		attributes: {
			'data-role' : 'fieldcontain'
		},
		template: '#scheduleTemplate'
	}); 

	var SchedulesViewList = Marionette.CollectionView.extend({
		tagName: 'ul',
		className: 'ui-listview ui-listview-inset ui-corner-all ui-shadow',
		emptyView: LoadingView,
		attributes: {
			'data-role': 'listview',
			'data-inset': true
		},
		itemView: SchedulesView,
		initialize: function() {
			this.collection.on('change', this.render, this);
		},
		onAfterItemAdded: function(itemView){
			this.$el.find(".deleteSchedule").buttonMarkup( { inline: true, icon: "delete"} );
	  }
	});

	var schedulesList =  new SchedulesViewList({
		collection: schedules
	});

	$('#scheduleContainer').html(schedulesList.render().$el);
}
function deleteSchedule(id)
{
	schedules.remove(schedules.get(id));
	$.post(
		server + 'schedules/destroy',
		{id:id},
		null
	);
}