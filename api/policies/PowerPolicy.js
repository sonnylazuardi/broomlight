module.exports = function (req, res, next) {
	if (req.session) {
		if (req.session.user) {
			var action = req.param('action');
			next();
		} else {
			res.send('You Must Be Logged In', 403);
		}
	} else {
		next();
	}
}