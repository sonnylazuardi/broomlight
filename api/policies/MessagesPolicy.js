module.exports = function (req, res, next) {
	if (req.session) {
		if (req.session.user) {
			var action = req.param('action');
			console.log(action);
			if (action == 'create') {
				var crypto = require('crypto');
				req.body.userId = req.session.user.id;
				req.body.groupId = req.session.user.group;
				req.body.username = req.session.user.username;
				req.body.image = "http://www.gravatar.com/avatar/"+crypto.createHash('md5').update(req.session.user.email).digest("hex")+"?s=50&r=G";
			}
			next();
		} else {
			res.send('You Must Be Logged In', 403);
		}
	} else {
		next();
	}
}