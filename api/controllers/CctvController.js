// api/controllers/CctvController.js

module.exports = {
    index: function (req, res) {
        if (req.session.user) 
            res.view({user: req.session.user});
        else
            res.redirect('/');
    },
    send: function (req,res) {
        // Get the value of a parameter
        var data = req.param('gambar');
        // res.broadcast('/cctv', );
        req.socket.broadcast.emit('cctv', data);
        // Send a JSON response
        // res.json({
        //   success: true,
        //   message: param
        // });
    },
};