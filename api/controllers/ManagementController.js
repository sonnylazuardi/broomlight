/*---------------------
	:: Management 
	-> controller
---------------------*/
var ManagementController = {
	management: function (req, res) {
		if (req.session.user) 
			res.view({user: req.session.user});
		else
			res.redirect('/');
	},
	formManagement: function (req, res) {
		if (req.session.user)  {
			res.view({user: req.session.user});
		} else {
			res.redirect('/');
		}
	},
	schedule: function (req, res) {
		if (req.session.user) 
			res.view({user: req.session.user});
		else
			res.redirect('/');
	},
	formSchedule: function (req, res) {
		if (req.session.user) {
			res.view({user: req.session.user});
		} else {
			res.redirect('/');
		}
	},
	electricity: function (req, res) {
		if (req.session.user) 
			res.view({user: req.session.user});
		else
			res.redirect('/');
	},
};
module.exports = ManagementController;