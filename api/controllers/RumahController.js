/*---------------------
	:: Rumah 
	-> controller
---------------------*/
var RumahController = {
	myProfile: function (req,res) {
  	if (!req.session.user) return res.redirect('/');
    Rumah.find(req.session.user.group)
    .done(function (err, myrumah) {
		  if (err) {
		    return console.log(err);
		  } else {
		  	var crypto = require('crypto');
				var profpic = "http://www.gravatar.com/avatar/"+crypto.createHash('md5').update(req.session.user.email).digest("hex")+"?s=300&r=G";
		  	res.json({user: req.session.user, rumah: myrumah, profpic: profpic});	
		  }
		});
  } 
};
module.exports = RumahController;