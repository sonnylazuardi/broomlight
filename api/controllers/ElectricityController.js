/*---------------------
	:: Electricity 
	-> controller
---------------------*/
var ElectricityController = {
	findAll: function(req, res) {
		if (!req.session.user) return res.send('Forbidden', 403);
		var options = {
			where: {groupId: req.session.user.group}
		};
		var finding = Electricity.findAll(options);

		finding.done(function afterFound(err, models) {
			if(err) return res.send(err, 500);
			if(!models) return res.send(404);
			if (!Electricity.silent) {
				Electricity.subscribe(req, models);
			}
			var modelValues = _.map(models, function (model) {
				var values;
				if (model && model.values) {
					values = model.values;
				}
				else values = model;
				return values;
			});
			return res.json(modelValues);
		});
	},
	create: function(req, res) {
		res.header('Access-Control-Allow-Origin', '*');
		var params = _.extend(req.query || {}, req.params || {}, req.body || {});
		Electricity.create({timestamp: params.timestamp, value: params.value, groupId: params.groupId}).done(function(err, result) {
			if (err) res.send(500, {error: 'DB Error'});
			else {
				var values;
				if (result && result.values) {
					values = result.values;
				}
				else values = result;
				if (!Electricity.silent) {
					Electricity.publishCreate(values);
				}
				return res.json(values);
			}
		});
	},
	detik: function(req, res) {
		var groupId = req.param('groupId');
		var timestamp = req.param('timestamp');
		var value = req.param('value');
		req.socket.broadcast.emit('detik', {"groupId": groupId, "timestamp": timestamp, "value": value});
	}
};
module.exports = ElectricityController;
