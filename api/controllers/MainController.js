/*---------------------
	:: Main 
	-> controller
---------------------*/
var cbUser = function(req, res, username, password, email, rumahId) {
	Users.create({username: username, password: password, group: rumahId, email: email}).done(function(error, user){
		if (error) {
			return res.send(500, {error: 'DB Error'});
		} else {
			req.session.user = user;
			Rumah.find(user.group)
	    .done(function (err, myrumah) {
			  if (err) {
			    return console.log(err);
			  } else {
			  	var crypto = require('crypto');
					var profpic = "http://www.gravatar.com/avatar/"+crypto.createHash('md5').update(user.email).digest("hex")+"?s=300&r=G";
			  	res.json({user: user, rumah: myrumah, profpic: profpic});	
			  }
			});
		}
	});
};
var MainController = {
	index: function (req, res) {
		if (req.session.user) return res.redirect('/menu');
		res.view();
	},
	signup: function (req, res) {
		var username = req.param('username');
		var password = req.param('password');
		var myrumah = req.param('myrumah');
		var mylat = req.param('mylat');
		var mylng = req.param('mylng');
		var email = req.param('email');
		var rumahId;
		Users.findByUsername(username).done(function(err,usr) {
			if (err) {
				res.send(500, {error: 'DB Error'});
			} else if (usr) {
				res.send(400, {error: 'Username already Taken'});
			} else {
				var hasher = require('password-hash');
				password = hasher.generate(password);
				Rumah.find({nama: myrumah})
					.done(function(err, result) {
					if (err) {
						return res.send(500, {error: 'DB Error'});
					} else {
						if (result) {
							rumahId = result.id;
							return cbUser(req, res, username, password, email, rumahId);
						} else {
							Rumah.create({nama: myrumah, lat: mylat, long: mylng, zoom: 15})
							.done(function(error, hasil){
								if (error) {
									return res.send(500, {error: 'DB Error'});
								} else {
									rumahId = hasil.id;		
									return cbUser(req, res, username, password, email, rumahId);
								}
							});	
						}
					}
				});
			}
		});
	},
	menu: function (req, res) {
		if (req.session.user) {
			var crypto = require('crypto');
			var profpic = "http://www.gravatar.com/avatar/"+crypto.createHash('md5').update(req.session.user.email).digest("hex")+"?s=300&r=G";
			res.view({username: req.session.user.username, profpic: profpic});	
		} else
			res.redirect('/');
	},
	login: function (req, res) {
  	res.header("Access-Control-Allow-Origin", "*");
  	res.header("Access-Control-Max-Age", "1728000");
  	res.header("Access-Control-Allow-Credentials", "true");
		var username = req.param('username');
		var password = req.param('password');
		if (username && password) {
			Users.findByUsername(username).done(function (err, usr) {
				if (err) {
					res.send(500, {error : 'DB Error'});
				} else {
					if (usr) {
						var hasher = require('password-hash');
						if (hasher.verify(password, usr.password)) {
							req.session.user = usr;
							Rumah.find(usr.group)
					    .done(function (err, myrumah) {
							  if (err) {
							    return console.log(err);
							  } else {
							  	var crypto = require('crypto');
									var profpic = "http://www.gravatar.com/avatar/"+crypto.createHash('md5').update(usr.email).digest("hex")+"?s=300&r=G";
							  	res.json({user: usr, rumah: myrumah, profpic: profpic});	
							  }
							});
						} else {
							res.send(400, {error: 'Wrong Password'});
						}
					} else {
						res.send(404, {error: 'User not found'});
					}
				}
			});
		} else res.send(404, {error: 'User/Password Kosong'});
	},
	logout: function (req, res) {
		delete req.session.user;
  	res.redirect('/');
	},
	chat: function (req, res) {
		if (req.session.user) {
			res.view({username: req.session.user.username});
		} else {
			res.redirect('/');
		}
	}
};
module.exports = MainController;