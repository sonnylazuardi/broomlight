/*---------------------
	:: Messages 
	-> controller
---------------------*/
var MessagesController = {
	// message sorted descendant
	findAll: function(req, res) {
		if (!req.session.user) return res.send(403, 'Forbidden');
		var options = {
			where: {groupId: req.session.user.group}
		};
		var finding = Messages.findAll(options);

		finding.done(function afterFound(err, models) {
			if(err) return res.send(err, 500);
			if(!models) return res.send(404);
			if (!Messages.silent) {
				Messages.subscribe(req, models);
			}
			var modelValues = _.map(models, function (model) {
				var values;
				if (model && model.values) {
					values = model.values;
				}
				else values = model;
				return values;
			});
			return res.json(modelValues);
		});
	},
  sorted: function (req,res) {
  	if (!req.session.user) return res.redirect('/');
    Messages.findAll().limit(10).sort('createdAt DESC').done(function (err, result) {
		  if (err) {
		    return console.log(err);
		  } else {
		    res.json(result);
		  }
		});
  }
};
module.exports = MessagesController;