/*---------------------
	:: Schedules 
	-> controller
---------------------*/
var SchedulesController = {
	findAll: function(req, res) {
		if (req.param('device') == 'controller') {
			var options = {
				where: {groupId: req.param('rumahId')}
			};
		} else {
			if (!req.session.user) return res.send(403, 'Forbidden');
			var options = {
				where: {groupId: req.session.user.group}
			};
		}
		var finding = Schedules.findAll(options);

		finding.done(function afterFound(err, models) {
			if(err) return res.send(err, 500);
			if(!models) return res.send(404);
			if (!Schedules.silent) {
				Schedules.subscribe(req, models);
			}
			var modelValues = _.map(models, function (model) {
				var values;
				if (model && model.values) {
					values = model.values;
				}
				else values = model;
				return values;
			});
			return res.json(modelValues);
		});
	},
	create: function(req, res) {
		if (!req.session.user) return res.send(403, 'Forbidden');
		var params = _.extend(req.query || {}, req.params || {}, req.body || {});
		params['portId'] = params['powerId'];
		delete params['powerId'];
		Power.find({portId: params.portId, groupId: req.session.user.group}).done(function(err, result) {
			if (!result || err) return res.send(404);
			if (!params.daily) params.daily = 0; else params.daily = 1;
			var newParam = {portId: params.portId, groupId: req.session.user.group, status: params.status, namaPower: result.name, jam: params.jam+':'+params.menit+':'+params.detik, daily: params.daily};
			Schedules.create(newParam, function(err, model) {
				if(err) return res.send(err, 500);

				var values;
				if (model && model.values) {
					values = model.values;
				}
				else values = model;
				if (!Schedules.silent) {
					Schedules.publishCreate(values);
				}
				return res.json(values);
			});
		});
	},
	destroy: function(req, res) {
		if (!req.session.user) return res.send(403, 'Forbidden');
		var params = _.extend(req.query || {}, req.params || {}, req.body || {});
		var id = req.param('id');
		if(!id) return res.send("No id provided.",404);
		else Schedules.find(id).done(function(err, model) {
			if(err) return res.send(err, 500);
			if(!model) return res.send(404);

			if (model.groupId != req.session.user.group)
				return res.send(403, 'Forbidden');
			Schedules.destroy(id, function(err) {
				if(err) return res.send(err, 500);
				var values;
				if (model && model.values) {
					values = model.values;
				}
				else values = model;
				if (!Schedules.silent) {
					Schedules.publishDestroy(values);
				}
			});
		});
	},
	destroyByGroup: function(req, res) {
		if (req.param('device')!='controller') return res.send(403, 'Forbidden');
		var params = _.extend(req.query || {}, req.params || {}, req.body || {});
		var portId = req.param('portId');
		var groupId = req.param('rumahId');

		Schedules.find({portId: portId, groupId: groupId}).done(function(err, result) {
			if (err || !result) res.send(404, 'Not Found');
			else {
				if (result.groupId != req.session.user.group)
					return res.send(403, 'Forbidden');
				Schedules.destroy(result.id, function(err) {
					if(err) return res.send(err, 500);
					var values;
					if (result && result.values) {
						values = result.values;
					}
					else values = result;
					if (!Schedules.silent) {
						Schedules.publishDestroy(values);
					}
				});
			}
		});
	}
};
module.exports = SchedulesController;
