/*---------------------
	:: Power 
	-> controller
---------------------*/
var PowerController = {
	findAll: function(req, res) {
		if (req.param('device') == 'controller') {
			var options = {
				where: {groupId: req.param('rumahId')}
			};
		} else {
			if (!req.session.user) return res.send(403, 'Forbidden');
			var options = {
				where: {groupId: req.session.user.group}
			};
		}
		var finding = Power.findAll(options);

		finding.done(function afterFound(err, models) {
			if(err) return res.send(err, 500);
			if(!models) return res.send(404);
			if (!Power.silent) {
				Power.subscribe(req, models);
			}
			var modelValues = _.map(models, function (model) {
				var values;
				if (model && model.values) {
					values = model.values;
				}
				else values = model;
				return values;
			});
			return res.json(modelValues);
		});
	},
	createUpdate: function(req, res) {
		if (!req.session.user) return res.send(403, 'Forbidden');
		var params = _.extend(req.query || {}, req.params || {}, req.body || {});
		Power.find({portId: params['portId'], groupId: req.session.user.group}).done(function(err, result) {
			if (err) return res.send(err, 500);
			if (!result) {
				createPower(req, res, params);
			} else {
				params['name'] = params['namePower'];
				delete params['namePower'];
				updatePower(req, res, result.id, params);
			}
		});
	},
	create: function(req, res) {
		if (!req.session.user) return res.send(403, 'Forbidden');
		res.header('Access-Control-Allow-Origin', '*');
		var params = _.extend(req.query || {}, req.params || {}, req.body || {});

		createPower(req, res, params);
	},
	update: function(req, res) {
		var id = req.param('id');
		if(!id) return res.send('No id specified!');
		var params = _.extend(req.query || {}, req.params || {}, req.body || {});
		
		updatePower(req, res, id, params);
	}
};
module.exports = PowerController;

function checkAccess (req, res, id, callback) {
	if (!req.session.user) return res.send('Forbidden');
	Power.find(id).done(function (err, result) {
		if (err || !result) return res.send('Forbidden');
		else {
			if (result.groupId != req.session.user.group) 
				return res.send('Forbidden');
			else
				callback();
		}
	})
}

function createPower (req, res, params) {
	Power.create({name: params.namePower, portId: params.portId, groupId: req.session.user.group, status: 0}).done(function(err, result) {
		if (err) res.send(500, {error: 'DB Error'});
		else  {
			var values;
			if (result && result.values) {
				values = result.values;
			}
			else values = result;
			if (!Power.silent) {
				Power.publishCreate(values);
			}
			return res.json(values);
		}
	});
}

function updatePower (req, res, id, params) {
	delete params['id'];
	delete params['action'];
	delete params['entity'];
	delete params['controller'];
	// checkAccess(req, res, id, function() {
		Power.update(id, params, function(err, result) {
			if(err) return res.send(err, 500);
			if(!result) return res.send('Power cannot be found.', 404);
			var values;
			if (result && result.values) {
				values = result.values;
			}
			else values = result;
			if (!Power.silent) {
				Power.publishUpdate(id, values);
			}
			return res.json(values);
		});	
	// })
}