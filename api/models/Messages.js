/*---------------------
	:: Messages
	-> model
---------------------*/
module.exports = {

	attributes	: {
		userId: 'INT',
		groupId: 'INT',
		username: 'STRING',
		image: 'STRING',
		message: 'STRING'
		// Simple attribute:
		// name: 'STRING',

		// Or for more flexibility:
		// phoneNumber: {
		//	type: 'STRING',
		//	defaultValue: '555-555-5555'
		// }
		
	}

};