/*---------------------
	:: Schedules
	-> model
---------------------*/
module.exports = {

	attributes	: {
		portId: 'INT',
		groupId: 'INT',
		jam: 'TIME',
		namaPower: 'STRING',
		status: 'INT',
		daily: 'INT'
		// Simple attribute:
		// name: 'STRING',

		// Or for more flexibility:
		// phoneNumber: {
		//	type: 'STRING',
		//	defaultValue: '555-555-5555'
		// }
		
	}

};