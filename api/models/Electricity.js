/*---------------------
	:: Electricity
	-> model
---------------------*/
module.exports = {

	attributes	: {
		timestamp: 'DATETIME',
		value: 'INT',
		groupId: 'INT'
		// Simple attribute:
		// name: 'STRING',

		// Or for more flexibility:
		// phoneNumber: {
		//	type: 'STRING',
		//	defaultValue: '555-555-5555'
		// }
		
	}

};