/*---------------------
	:: Rumah
	-> model
---------------------*/
module.exports = {

	attributes	: {
		nama: 'STRING',
		lat: 'STRING',
		long: 'STRING',
		zoom: 'STRING',
		// Simple attribute:
		// name: 'STRING',

		// Or for more flexibility:
		// phoneNumber: {
		//	type: 'STRING',
		//	defaultValue: '555-555-5555'
		// }
		
	}

};