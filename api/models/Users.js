/*---------------------
	:: Users
	-> model
---------------------*/
module.exports = {

	attributes	: {
		username: 'STRING',
		password: 'STRING',
		email: 'STRING',
		group: 'INT',
		online: 'INT'
		// Simple attribute:
		// name: 'STRING',

		// Or for more flexibility:
		// phoneNumber: {
		//	type: 'STRING',
		//	defaultValue: '555-555-5555'
		// }
		
	}

};