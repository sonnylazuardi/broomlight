/*---------------------
	:: Power
	-> model
---------------------*/
module.exports = {

	attributes	: {
		groupId: 'INT',
		name: 'STRING',
		portId: 'INT',
		status: 'INT'
		// Simple attribute:
		// name: 'STRING',

		// Or for more flexibility:
		// phoneNumber: {
		//	type: 'STRING',
		//	defaultValue: '555-555-5555'
		// }
		
	}

};